import React, { Component } from "react";
import { Link } from 'react-router-dom';

const Menu = () => (
<>
<nav className="navbar navbar-expand-md navbar-light bg-light ">

		    <a className="navbar-brand" href="#">Reconocimiento de mascotas</a>
		    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		     	<span className="navbar-toggler-icon"></span>
		    </button>
		    <div className="collapse navbar-collapse" id="navbarCollapse">
			    <ul className="navbar-nav mr-auto">
			        <li className="nav-item active">
			          	 <Link className="nav-link" to="/">Home</Link>
   </li>
			        <li className="nav-item">
			          	   <Link className="nav-link" to="/MascotaPerdida">Perdidas</Link>
			        </li>
			        <li className="nav-item">
  <Link className="nav-link" to="/MascotaEncontrada">Perdidas</Link>

			        </li>
			        <li className="nav-item">
			           <Link className="nav-link" to="/entrenar">Entrenar</Link>
			        </li>
			    </ul>

		    </div>

		</nav>

 </>
    );
export default Menu;
