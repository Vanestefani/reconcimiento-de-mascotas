import React from "react";
import './App.css';
import Home from './Paginas/home';
import MascotaPerdida from './Paginas/mascotas-perdidas';
import MascotaEncontrada from './Paginas/mascotas-encontradas';
import Entrenar from './Paginas/entrenar';
import Paginaerror from './Paginas/paginanoencontrada';

import {  BrowserRouter as Router,  Route,  Switch,} from 'react-router-dom';

function App() {
  return (

    <Router>

    <Switch>
                <Route path="/" component={Home} exact />
                <Route path="/MascotaPerdida" component={MascotaPerdida} />
                <Route path="/MascotaEncontrada" component={MascotaEncontrada} />
                <Route path="/entrenar" component={Entrenar} />
                <Route  component={Paginaerror}  />
     </Switch>
    </Router>

  );
}

export default App;
